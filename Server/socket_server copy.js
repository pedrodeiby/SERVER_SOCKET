import { PORTclient, PORT } from './config.js'
import http from 'http'
import { Server as socketio } from 'socket.io'

const servidor = http.createServer()
const IO = new socketio(servidor, {
    cors: {
        //origin: 'http://localhost:'+PORTclient
        origin: '*'
    }
})

IO.on('connection', (SOCKET) => {
    
    console.log(`User ${SOCKET.id} conectado`)

    //ROOMS ADMIN GREMIALS
    SOCKET.on('JoinRoomGremial', (idGremial) => {
        SOCKET.join(['gremial_line'+idGremial]);
    })
    SOCKET.on('LeaveRoomDriver', (idGremial) => {
        SOCKET.leave('gremial_line'+idGremial);
    })

    //ROOMS DRIVERS
    SOCKET.on('JoinRoomDriver', (idLine, idDriver) => {
        SOCKET.join(['driver_line' + idLine]);
    })
    SOCKET.on('LeaveRoomDriver', (idLine, idDriver) => {
        SOCKET.leave('driver_line' + idLine);
    })

    //ROOMS PASSENGERS
    SOCKET.on('JoinRoomPassenger', (idLine, idPassenger = null) => {
        SOCKET.join(['passenger_line'+idLine]);
        console.log(`Pasajero ${idPassenger?idPassenger:"desconocido"} se unio a la linea con ID:${idLine}`);
    })
    SOCKET.on('LeaveRoomPassenger', (idLine, idPassenger = null) => {
        SOCKET.leave('passenger_line'+idLine);
    })

    //LOCATIONS
    SOCKET.on('StartLocation', (idLine, idVehicle, idDriver) => {
        SOCKET.to('gremial_line'+idLine).emit('StartLocation', idVehicle, idDriver)
        SOCKET.to('passenger_line'+idLine).emit('StartLocation')
    })
    SOCKET.on('UpdateLocation', (idLine, idVehicle, idDriver, locationJson) => {
        console.log(locationJson);
        SOCKET.to('gremial_line'+idLine).emit('UpdateLocation', idVehicle, idDriver, locationJson)
        SOCKET.to('passenger_line'+idLine).emit('UpdateLocation', idVehicle, locationJson)
    })
    SOCKET.on('StopLocation', (idLine, idVehicle, idDriver) => {
        SOCKET.to('gremial_line'+idLine).emit('StopLocation', idVehicle, idDriver)
        SOCKET.to('passenger_line'+idLine).emit('StopLocation', idVehicle)
    })

    SOCKET.on('disconnect', ()=>{
        console.log(`User ${SOCKET.id} desconectado`);
        // IO.emit('chat',SOCKET.id+": desconectado");
    })

})

servidor.listen(PORT, () => {
    console.log('Servidor corriendo en http://localhost:' + PORT)
})