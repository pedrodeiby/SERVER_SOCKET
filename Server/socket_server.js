import { PORTclient, PORT } from './config.js'
import http from 'http'
import { Server as socketio } from 'socket.io'

const servidor = http.createServer()
const IO = new socketio(servidor, {
    cors: {
        //origin: 'http://localhost:'+PORTclient
        origin: '*'
    }
})

IO.on('connection', (SOCKET) => {

    console.log(`User ${SOCKET.id} conectado`)

    //#region ROOMS ADMIN GREMIALS
    SOCKET.on('JoinRoomAdmin', (idLine, route, idAdmin) => {
        SOCKET.role = 'admin';
        SOCKET.idLine = idLine;
        SOCKET.route = route; // 'A' o 'B'
        SOCKET.idAdmin = idAdmin;
        SOCKET.join(['admin_line' + SOCKET.idLine + SOCKET.route]);
        console.log(`Administrador ${SOCKET.idAdmin} se unio a la linea con ID:${SOCKET.idLine} ruta ${SOCKET.route}`);
    })
    function LeaveRoomAdmin(SOCKET) {
        if (SOCKET.idLine != null) {
            console.log(`Administrador ${SOCKET.idAdmin} se desconecto de la linea con ID ${SOCKET.idLine} ruta ${SOCKET.route}`);
            SOCKET.leave('admin_line' + SOCKET.idLine + SOCKET.route);
            SOCKET.idLine = null;
            SOCKET.route = null;
        }
    }
    SOCKET.on('LeaveRoomAdmin', () => {
        LeaveRoomAdmin(SOCKET);
    })
    //#endregion

    //#region ROOMS DRIVERS
    SOCKET.on('JoinRoomDriver', (idLine, route, idVehicle, idDriver) => {
        SOCKET.role = 'driver';
        SOCKET.idLine = idLine;
        SOCKET.route = route; // 'A' o 'B'
        SOCKET.idVehicle = idVehicle;
        SOCKET.idDriver = idDriver;
        SOCKET.join(['driver_line' + SOCKET.idLine + SOCKET.route]);
        console.log(`Conductor ${SOCKET.idDriver} se unio a la linea con ID ${SOCKET.idLine} ruta ${SOCKET.route}`);
    })
    function LeaveRoomDriver(SOCKET) {
        if (SOCKET.idLine != null) {
            console.log(`Conductor ${SOCKET.idDriver} se desconecto de la linea con ID ${SOCKET.idLine} ruta ${SOCKET.route}`);
            SOCKET.leave('driver_line' + SOCKET.idLine + SOCKET.route);
            SOCKET.idLine = null;
            SOCKET.route = null;
            SOCKET.idVehicle = null;
        }
    }
    SOCKET.on('LeaveRoomDriver', () => {
        LeaveRoomDriver(SOCKET);
    })
    //#endregion

    //#region ROOMS PASSENGERS
    SOCKET.on('JoinRoomPassenger', (idLine, route, idPassenger) => {
        SOCKET.role = 'passenger';
        SOCKET.idLine = idLine;
        SOCKET.route = route; // 'A' o 'B‘
        SOCKET.idPassenger = idPassenger;
        SOCKET.join(['passenger_line' + SOCKET.idLine + SOCKET.route]);
        SOCKET.to('driver_line' + SOCKET.idLine + SOCKET.route).emit('OnlinePassenger');
        SOCKET.to('passenger_line' + SOCKET.idLine + SOCKET.route).emit('OnlinePassenger');
        console.log(`Pasajero ${SOCKET.idPassenger ? SOCKET.idPassenger : "desconocido"} se unio a la linea con ID ${SOCKET.idLine} ruta ${SOCKET.route}`);
    })
    function LeaveRoomPassenger(SOCKET) {
        if (SOCKET.idLine != null) {
            console.log(`Pasajero ${SOCKET.idPassenger ? SOCKET.idPassenger : "desconocido"} se desconecto de la linea con ID ${SOCKET.idLine} ruta ${SOCKET.route}`);
            SOCKET.leave('passenger_line' + SOCKET.idLine + SOCKET.route);
            SOCKET.idLine = null;
            SOCKET.route = null;
            SOCKET.to('driver_line' + SOCKET.idLine + SOCKET.route).emit('OfflinePassenger');
            SOCKET.to('passenger_line' + SOCKET.idLine + SOCKET.route).emit('OfflinePassenger');
        }
    }
    SOCKET.on('LeaveRoomPassenger', () => {
        LeaveRoomPassenger(SOCKET);
    })
    //#endregion

    //#region LOCATIONS
    SOCKET.on('StartLocation', () => {
        SOCKET.to('admin_line' + SOCKET.idLine + SOCKET.route).emit('StartLocation', SOCKET.idVehicle, SOCKET.idDriver)
        SOCKET.to('passenger_line' + SOCKET.idLine + SOCKET.route).emit('StartLocation')
        SOCKET.to('driver_line' + SOCKET.idLine + SOCKET.route).emit('OnlineDriver');
        console.log(`Conductor ${SOCKET.idDriver} empezo a transmitir en la linea con ID ${SOCKET.idLine} ruta ${SOCKET.route}`);
    })
    SOCKET.on('UpdateLocation', (locationJson) => {
        SOCKET.to('admin_line' + SOCKET.idLine + SOCKET.route).emit('UpdateLocation', SOCKET.idVehicle, SOCKET.idDriver, locationJson)
        SOCKET.to('passenger_line' + SOCKET.idLine + SOCKET.route).emit('UpdateLocation', SOCKET.idVehicle, locationJson)
        console.log(`Conductor ${SOCKET.idDriver} transmitiendo ${locationJson.requestNumber}`);
    })
    SOCKET.on('StopLocation', () => {
        StopLocation(SOCKET)
    })
    function StopLocation(SOCKET) {
        if (SOCKET.idLine != null) {
            SOCKET.to('admin_line' + SOCKET.idLine + SOCKET.route).emit('StopLocation', SOCKET.idVehicle, SOCKET.idDriver);
            SOCKET.to('passenger_line' + SOCKET.idLine + SOCKET.route).emit('StopLocation', SOCKET.idVehicle);
            SOCKET.to('driver_line' + SOCKET.idLine + SOCKET.route).emit('OfflineDriver');
            console.log(`Conductor ${SOCKET.idDriver} dejo de transmitir en la linea con ID ${SOCKET.idLine} ruta ${SOCKET.route}`);
        }
    }

    //#endregion


    SOCKET.on('disconnect', () => {
        if (SOCKET.role == 'admin') {
            LeaveRoomAdmin(SOCKET);
        } else if (SOCKET.role == 'driver') {
            StopLocation(SOCKET);
            LeaveRoomDriver(SOCKET);
        } else if (SOCKET.role == 'passenger') {
            LeaveRoomPassenger(SOCKET);
        }
        else {
            console.log(`User ${SOCKET.id} desconectado`)
        }
    })

})

servidor.listen(PORT, () => {
    console.log('Servidor corriendo en http://localhost:' + PORT)
})